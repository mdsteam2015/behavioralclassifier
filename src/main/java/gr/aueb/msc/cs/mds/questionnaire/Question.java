/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.msc.cs.mds.questionnaire;

/**
 *
 * @author nick
 */
public class Question {
    String id;
    String qNum;
    float priceA, priceB;
    int distA, distB;

    public Question(String id, String qNum, String valsA, String valsB) {
        this.id = id;
        this.qNum = qNum;
        String[] tokensA, tokensB;
        
        tokensA = valsA.split("-");
        tokensB = valsB.split("-");
        
        priceA = Float.parseFloat(tokensA[0]);
        priceB = Float.parseFloat(tokensB[0]);
        distA = Integer.parseInt(tokensA[1]);
        distB = Integer.parseInt(tokensB[1]);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getqNum() {
        return qNum;
    }

    public void setqNum(String qNum) {
        this.qNum = qNum;
    }

    public float getPriceA() {
        return priceA;
    }

    public void setPriceA(float priceA) {
        this.priceA = priceA;
    }

    public float getPriceB() {
        return priceB;
    }

    public void setPriceB(float priceB) {
        this.priceB = priceB;
    }

    public int getDistA() {
        return distA;
    }

    public void setDistA(int distA) {
        this.distA = distA;
    }

    public int getDistB() {
        return distB;
    }

    public void setDistB(int distB) {
        this.distB = distB;
    }
    
    
}
