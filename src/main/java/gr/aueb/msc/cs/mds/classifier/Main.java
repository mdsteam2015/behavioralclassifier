/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.msc.cs.mds.classifier;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import gr.aueb.msc.cs.mds.questionnaire.JSONAnswer;
import gr.aueb.msc.cs.mds.questionnaire.Question;
import gr.aueb.msc.cs.mds.questionnaire.Submission;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.channels.FileLock;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Nick
 */
public class Main {

    // If serialization hash breaks
    // private static final long serialVersionUID = 1L;
    private static org.slf4j.Logger Log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        HashMap<String, Question> ques = new HashMap<>();

        System.out.println("\n\n------------------------ Behavioral Science Project --------------------------\n\n");
        Log.info("LibSVM Test {}", args.length);
        Log.info("Weka Test {}", args.length);
        Log.info("Logging Test {}", args.length);
        Log.error("Error Test {}", args.length);

        File dir = new File("src/main/resources/questions");
        List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        for (File file : files) {
            String q_ID = "";
            String q_AN = "";
            String[] q_Vals = new String[2];

            //Question q = new Question();
            //Log.info("file: " + file.getCanonicalPath());
            JsonFactory f = new MappingJsonFactory();
            JsonParser jp = f.createJsonParser(file);

            JsonToken current;

            current = jp.nextToken();
            if (current != JsonToken.START_OBJECT) {
                System.out.println("Error: root should be object: quiting.");
                return;
            }

            while (jp.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = jp.getCurrentName();

                if (fieldName == null) {
                    continue;
                }

                // move from field name to field value
                current = jp.nextToken();
                //Log.info(fieldName);

                if (fieldName.equals("id")) {
                    //Log.info(""+jp.getValueAsString());
                    q_ID = jp.getValueAsString();
                } else if (fieldName.equals("question")) {
                    q_AN = jp.getValueAsString();
                } else {
                    if (current == JsonToken.START_ARRAY) {
                        // For each of the records in the array
                        int i = 0;
                        while (jp.nextToken() != JsonToken.END_ARRAY) {
                            // read the record into a tree model,
                            // this moves the parsing position to the end of it
                            JsonNode node = jp.readValueAsTree();
                            // And now we have random access to everything in the object
                            //Log.info("field "+i+": " + node.asText());
                            q_Vals[i++] = node.asText();
                        }
                    }
                }
            }
            Question q = new Question(q_ID, q_AN, q_Vals[0], q_Vals[1]);
            ques.put(q_ID, q);
        }

        for (Map.Entry<String, Question> en : ques.entrySet()) {
            //Log.info(en.getKey());
            //Log.info(""+en.getValue().getDistB());
        }
        Log.info("Size ");
        File profiles = new File("./profiles");
        profiles.mkdirs();

        Log.info("Size ");
        List<Submission> resultList = null;
        File resultsFile = new File("src/main/resources/results.db");
        if (!resultsFile.exists()) {
            Log.error("Results DB not found");
        } else {
            Log.info("Size ");
            FileInputStream fis = new FileInputStream(resultsFile);
            try {
                FileLock lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true);
                while (lock == null) {
                    lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true);
                }
                try {
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    resultList = (List<Submission>) ois.readObject();
                } finally {
                    lock.release();
                }
            } finally {
                fis.close();
            }

            Log.info("Size " + resultList.size());
            for (Submission sub : resultList) {
                File profileFile = new File("profiles/profile_" + sub.getId() + ".csv");
                FileOutputStream fosb = new FileOutputStream(profileFile, false);
                PrintWriter pw = null;
                FileLock lock = fosb.getChannel().tryLock();
                while (lock == null) {
                    lock = fosb.getChannel().tryLock();
                }
                try {
                    pw = new PrintWriter(fosb);
                    pw.print("PROFILE_ID" + ",");
                    pw.print("PRICE_A" + ",");
                    pw.print("DISTANCE_A" + ",");
                    pw.print("PRICE_B" + ",");
                    pw.print("DISTANCE_B" + ",");
                    pw.println("ANSWER");
                    for (JSONAnswer an : sub.getAns()) {
                        Question q = ques.get(Integer.toString(an.getId()));
                        pw.print(sub.getId() + ",");
                        pw.print(q.getPriceA() + ",");
                        pw.print(q.getDistA() + ",");
                        pw.print(q.getPriceB() + ",");
                        pw.print(q.getDistB() + ",");
                        pw.println(an.getVal());
                    }
                } finally {
                    lock.release();
                }

                if (pw != null) {
                    pw.close();
                }

                fosb.close();
            }
        }

        /*
         // Preparing the SVM param
         svm_parameter param=new svm_parameter();
         param.svm_type=svm_parameter.C_SVC;
         param.kernel_type=svm_parameter.RBF;
         param.gamma=0.5;
         param.nu=0.5;
         param.cache_size=20000;
         param.C=1;
         param.eps=0.001;
         param.p=0.1;

         HashMap<Integer, HashMap<Integer, Double>> featuresTraining=new HashMap<Integer, HashMap<Integer, Double>>();
         HashMap<Integer, Integer> labelTraining=new HashMap<Integer, Integer>();
         HashMap<Integer, HashMap<Integer, Double>> featuresTesting=new HashMap<Integer, HashMap<Integer, Double>>();

         HashSet<Integer> features=new HashSet<Integer>();

         //Read in training data
         BufferedReader reader=null;
         try{
         reader=new BufferedReader(new FileReader("a1a.train"));
         String line=null;
         int lineNum=0;
         while((line=reader.readLine())!=null){
         featuresTraining.put(lineNum, new HashMap<Integer,Double>());
         String[] tokens=line.split("\\s+");
         int label=Integer.parseInt(tokens[0]);
         labelTraining.put(lineNum, label);
         for(int i=1;i<tokens.length;i++){
         String[] fields=tokens[i].split(":");
         int featureId=Integer.parseInt(fields[0]);
         double featureValue=Double.parseDouble(fields[1]);
         features.add(featureId);
         featuresTraining.get(lineNum).put(featureId, featureValue);
         }
         lineNum++;
         }

         reader.close();
         }catch (IOException | NumberFormatException e){
         if ( e instanceof IOException ) {
                
         } else if ( e instanceof NumberFormatException ) {
                
         }
         }

         //Read in test data
         try{
         reader=new BufferedReader(new FileReader("a1a.t"));
         String line=null;
         int lineNum=0;
         while((line=reader.readLine())!=null){

         featuresTesting.put(lineNum, new HashMap<Integer,Double>());
         String[] tokens=line.split("\\s+");
         for(int i=1; i<tokens.length;i++){
         String[] fields=tokens[i].split(":");
         int featureId=Integer.parseInt(fields[0]);
         double featureValue=Double.parseDouble(fields[1]);
         featuresTesting.get(lineNum).put(featureId, featureValue);
         }
         lineNum++;
         }
         reader.close();
         }catch (Exception e){

         }

         //Train the SVM model
         svm_problem prob=new svm_problem();
         int numTrainingInstances=featuresTraining.keySet().size();
         prob.l=numTrainingInstances;
         prob.y=new double[prob.l];
         prob.x=new svm_node[prob.l][];

         for(int i=0;i<numTrainingInstances;i++){
         HashMap<Integer,Double> tmp=featuresTraining.get(i);
         prob.x[i]=new svm_node[tmp.keySet().size()];
         int indx=0;
         for(Integer id:tmp.keySet()){
         svm_node node=new svm_node();
         node.index=id;
         node.value=tmp.get(id);
         prob.x[i][indx]=node;
         indx++;
         }

         prob.y[i]=labelTraining.get(i);
         }

         svm_model model=svm.svm_train(prob,param);

         for(Integer testInstance:featuresTesting.keySet()){
         HashMap<Integer, Double> tmp=new HashMap<Integer, Double>();
         int numFeatures=tmp.keySet().size();
         svm_node[] x=new svm_node[numFeatures];
         int featureIndx=0;
         for(Integer feature:tmp.keySet()){
         x[featureIndx]=new svm_node();
         x[featureIndx].index=feature;
         x[featureIndx].value=tmp.get(feature);
         featureIndx++;
         }

         double d=svm.svm_predict(model, x);

         System.out.println(testInstance+"\t"+d);
         }
         */
    }
}